﻿using Softtek.Academy2018.SurveyApp.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using System.Data.Entity;

namespace Softtek.Academy2018.SurveyApp.Data.Implementations
{
    public class OptionDataRepository : IOptionRepository
    {
        public int Add(Option option)
        {
            using (var ctx = new SurveyDbContext())
            {

                option.CreatedDate = DateTime.Now;
                option.ModifiedDate = null;

                ctx.Options.Add(option);

                ctx.SaveChanges();

                return option.Id;
            }
        }

        public bool Delete(Option option)
        {
            throw new NotImplementedException();
        }

        public bool Exist(string text)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Options.AsNoTracking().Any(x => x.Text.ToLower() == text.ToLower());
            }
        }

        public Option Get(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Options.AsNoTracking().SingleOrDefault(x => x.Id == id);
            }
        }

        public ICollection<Option> GetAll()
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Options.ToList();
            }
        }

        public string GetText(int id)
        {
            using (var ctx = new SurveyDbContext())
            {
                return ctx.Options.SingleOrDefault(x => x.Id == id).Text ?? null;
            }
        }

        public bool Update(Option option)
        {
            using (var ctx = new SurveyDbContext())
            {
                Option currentOption = ctx.Options.SingleOrDefault(x => x.Id == option.Id);

                if (currentOption == null) return false;

                currentOption.Text = option.Text;
                currentOption.ModifiedDate = DateTime.Now;


                ctx.Entry(currentOption).State = EntityState.Modified;

                ctx.SaveChanges();

                return true;
            }
        }
    }
}
