﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Softtek.Academy2018.SurveyApp.WebAPI.Controllers
{
    [RoutePrefix("api/option")]
    public class OptionController : ApiController
    {
        private readonly IOptionService _optionService;

        public OptionController(IOptionService optionService)
        {
            _optionService = optionService;
        }



        [Route("")]
        [HttpPost]
        public IHttpActionResult Create([FromBody] OptionDTO optionDTO)
        {
            if (optionDTO == null) return BadRequest("Request is null");

            Option option = new Option
            {
                Text = optionDTO.Text,
                CreatedDate = optionDTO.CreatedDate,
                ModifiedDate = optionDTO.ModifiedDate
            };

            int id = _optionService.Add(option);

            if (id <= 0) return BadRequest("Unable to create option");

            var payload = new { OptionId = id };

            return Ok(payload);
        }

        [Route("{id:int}")]
        [HttpPut]
        public IHttpActionResult UpdateOption([FromUri] int id, [FromBody] OptionDTO optionDTO)
        {
            if (optionDTO == null) return BadRequest("Option is null");
            Option option = new Option
            {
                Id = id,
                Text = optionDTO.Text
            };

            bool result = _optionService.Update(option);

            if (!result) return BadRequest("Unable to update option");
            return Ok();
        }


        [Route("~/api/options")]
        [HttpGet]
        public IHttpActionResult GetOptions()
        {
            ICollection<Option> options = _optionService.GetAll();
            if (options == null) return BadRequest("Request is null");

            ICollection<OptionDTO> optionList = options.Select(x => new OptionDTO
            {
                Id = x.Id,
                Text = x.Text
            }).ToList();

            return Ok(optionList);

        }
    }
}
