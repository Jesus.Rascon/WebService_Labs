﻿using Softtek.Academy2018.SurveyApp.Business.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softtek.Academy2018.SurveyApp.Domain.Model;
using Softtek.Academy2018.SurveyApp.Data.Contracts;

namespace Softtek.Academy2018.SurveyApp.Business.Implementations
{
    public class OptionService : IOptionService
    {
        private readonly IOptionRepository _optionRepository;

        public OptionService(IOptionRepository optionRepository)
        {
            _optionRepository = optionRepository;
        }

        public int Add(Option option)
        {
            if (string.IsNullOrEmpty(option.Text) ||
                option.Text.Length > 500) return 0;

            bool isExist = _optionRepository.Exist(option.Text);

            if (isExist) return 0;

            
            int id = _optionRepository.Add(option);

            return id;
        }

        public Option Get(int id)
        {
            if (id <= 0) return null;

            Option option = _optionRepository.Get(id);

            if (option != null) return null;

            return option;
        }

        public ICollection<Option> GetAll()
        {
            return _optionRepository.GetAll();
        }

        public bool Update(Option option)
        {
            if (option.Id <= 0) return false;

            if (string.IsNullOrEmpty(option.Text) ||
               option.Text.Length > 500) return false;

            string currentText = _optionRepository.GetText(option.Id);

            if (string.IsNullOrEmpty(currentText)) return false;

            bool existText = _optionRepository.Exist(option.Text);

            if (existText && currentText != option.Text) return false;

            return _optionRepository.Update(option);
        }
    }
}
